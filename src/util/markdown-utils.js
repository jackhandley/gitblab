import ReactMarkdown from 'react-markdown';

function Markdown({markdown}) {
  return <ReactMarkdown>{markdown}</ReactMarkdown>
}


export {
  Markdown,
}